package requestutil

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/pat"
	"io/ioutil"
	"net"
	"net/http"
	"bufio"
)

type CORSHandler struct {
	Router      *pat.Router
	Domain      string
	Credentials bool
}

func (self *CORSHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	AddCorsHeaders(w, self.Domain, self.Credentials)
	if r.Method == "OPTIONS" {
		http.Redirect(w, r, r.RequestURI, 200)
		return
	}
	self.Router.ServeHTTP(w, r)
}

// Authorize Cross domain origins on the specific domain.
func AddCorsHeaders(w http.ResponseWriter, domain string, credentials bool) {
	w.Header().Add("Access-Control-Allow-Origin", domain)
	w.Header().Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Add("Access-Control-Allow-Headers", "content-Type, authorization, accept")
	w.Header().Add("Access-Control-Max-Age", "604800");
	if credentials {
		w.Header().Add("Access-Control-Allow-Credentials", "true")
	}
}

// An Error type which contains an error written in a special way when marshalled.
type Error struct {
	Error string `json:"error"`
	StatusCode int `json:"statusCode,omitempty"`
}

func NewError(err error, status int) *Error { return &Error{err.Error(), status} }

func GetObjectFromRequest(request *http.Request, object interface{}) error {
	jsonString, err := GetJSONFromRequest(request)
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(jsonString), object)
	return err
}

func GetObjectFromResponse(response *http.Response, object interface{}) error {
	jsonString, err := GetJSONFromResponse(response)
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(jsonString), object)
	return err
}

func GetObjectFromRequestDebug(request *http.Request, object interface{}) error {
	jsonString, err := GetJSONFromRequest(request)
	fmt.Println(jsonString)
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(jsonString), object)
	return err
}

func GetObjectFromResponseDebug(response *http.Response, object interface{}) error {
	jsonString, err := GetJSONFromResponse(response)
	fmt.Println(jsonString)
	if err != nil {
		return err
	}
	err = json.Unmarshal([]byte(jsonString), object)
	return err
}
func GetJSONFromResponseDebug(response *http.Response) (string, error) {
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	response.Body.Close()
	fmt.Println(string(body))
	return string(body), nil
}

func GetJSONFromResponse(response *http.Response) (string, error) {
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	response.Body.Close()
	return string(body), nil
}

func GetJSONFromRequestDebug(request *http.Request) (string, error) {
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return "", err
	}
	request.Body.Close()
	fmt.Println(string(body))
	return string(body), nil
}

func GetJSONFromRequest(request *http.Request) (string, error) {
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		return "", err
	}
	request.Body.Close()
	return string(body), nil
}

// A ResponseWriter adapted to JSON
type ResponseWriterJson struct {
	http.ResponseWriter
	http.Hijacker
}

// Marshal an object and write
func (self ResponseWriterJson) WriteJson(object interface{}) {
	jsonBytes, err := json.Marshal(object)
	if err != nil {
		self.WriteError(err, 401)
		return
	}
	self.Write(jsonBytes)
}

func (self ResponseWriterJson) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	return self.ResponseWriter.(http.Hijacker).Hijack()
}

// Write a single string in a JSON way
func (self ResponseWriterJson) WriteSingleStringJson(key, value string) {
	self.Write([]byte(fmt.Sprintf("{\"%v\": \"%v\"}", key, value)))
}

// Send a Response with an error in a JSON way.
func (self ResponseWriterJson) WriteError(err error, errorCode int) {
	b, _ := json.MarshalIndent(NewError(err, 0), "", "  ")
	if errorCode != 0 {
		http.Error(self, string(b), errorCode)
	} else {
		http.Error(self, string(b), 400)
	}
}

// Send a Response with an error in a JSON way with an HTTP ERROR code and a statusCode
func (self ResponseWriterJson) WriteErrorStatus(err *Error, errorCode int) {
	b, _ := json.MarshalIndent(err, "", "  ")
	if errorCode != 0 {
		http.Error(self, string(b), errorCode)
	} else {
		http.Error(self, string(b), errorCode)
	}
}

// Wrap a simple HandlerFunc to replace the simple writer by
// a ResponseWriterJson
func HandlerWrapper(handler func(ResponseWriterJson, *http.Request) (error, int)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err, code := handler(ResponseWriterJson{w, w.(http.Hijacker)}, r); err != nil {
			ResponseWriterJson{w, w.(http.Hijacker)}.WriteError(err, code)
		}
	}
}

func HandlerErrorWrapper(handler func(ResponseWriterJson, *http.Request) (*Error, int)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err, code := handler(ResponseWriterJson{w, w.(http.Hijacker)}, r); err != nil {
			ResponseWriterJson{w, w.(http.Hijacker)}.WriteErrorStatus(err, code)
		}
	}
}
